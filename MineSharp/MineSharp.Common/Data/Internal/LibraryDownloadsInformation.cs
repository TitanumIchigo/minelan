namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about library downloads
    /// </summary>
    public class LibraryDownloadsInformation
    {
        /// <summary>
        /// Library artifact, null if library is native
        /// </summary>
        public LibraryArtifactInformation Artifact;
        
        /// <summary>
        /// Library classifiers for natives
        /// </summary>
        public LibraryClassifierInformation Classifiers;

        /// <summary>
        /// Returns true if library is native one
        /// </summary>
        public bool IsNative
        {
            get { return Artifact == null; }
        }

        /// <summary>
        /// Gets Library Artifact
        /// </summary>
        /// <returns>Library Artifact Information</returns>
        public LibraryArtifactInformation GetArtifact()
        {
            //TODO: Linux & OSX Support
            if (Artifact != null) return Artifact;
            else return Classifiers.Natives_windows;
        }

        /// <summary>
        /// Gets Library native Artifact (if exists)
        /// </summary>
        /// <returns>Artifact or null if does not exist</returns>
        public LibraryArtifactInformation TryGetNativesArtifact()
        {
            //TODO: Linux & OSX Support
            return Classifiers?.Natives_windows;
        }
    }
}