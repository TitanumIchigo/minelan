
namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Represents Game Asset Index Information,
    /// which is available in Version Manifest
    /// </summary>
    public class AssetIndex
    {
        /// <summary>
        /// Asset ID
        /// </summary>
        public string Id;
        
        /// <summary>
        /// Asset checksum
        /// </summary>
        public string Sha1;
        
        /// <summary>
        /// Asset size
        /// </summary>
        public long Size;
        
        /// <summary>
        /// Asset total size
        /// </summary>
        public long TotalSize;
        
        /// <summary>
        /// Asset URI
        /// </summary>
        public string Url;

    }
}