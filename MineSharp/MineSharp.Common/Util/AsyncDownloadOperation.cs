namespace MineSharp.Common.Util
{
    /// <summary>
    /// Async Download Operation Object
    /// represents operation that is performed in background
    /// </summary>
    public class AsyncDownloadOperation
    {

        /// <summary>
        /// Currently executed operation eg. library name
        /// </summary>
        public string Operation;
        
        /// <summary>
        /// Currently passed operations
        /// </summary>
        public int Current;
        
        /// <summary>
        /// Total download operations to pass
        /// </summary>
        public int Total;

        /// <summary>
        /// True if operation has been completed (Current == Total)
        /// </summary>
        public bool Completed
        {
            get { return Current == Total; }
        }
    }
}